class Doctor < ApplicationRecord
  has_secure_password
  
  belongs_to :department, dependent: :destroy
  has_many :appointments
  
  has_many :schedules
  
  validates :name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i },
                    uniqueness: true
  validates :specialization, length: { maximum: 50}
  validates :contact, presence: true, length: {  maximum: 16 },
                            format: { with: /\A[0-9]+\z/ },
                            uniqueness: true
  validates :role, presence: true
  
  enum role: {
    admin: 1,
    doctor: 2
  }

  def new_attributes
    {
          id: self.id,
          name: self.name,
          email: self.email,
          specialization: self.specialization,
          contact: self.contact,
          department_id: self.department_id,
          role: self.role,
          created_at: self.created_at
    }
      # if self.role == 'admin'
      #   {
      #     id: self.id,
      #     name: self.name,
      #     email: self.email,
      #     contact: self.contact,
      #     department_id: self.department_id,
      #     role: self.role,
      #     created_at: self.created_at
      #   }
      # else
      #   {
      #     id: self.id,
      #     name: self.name,
      #     email: self.email,
      #     specialization: self.specialization,
      #     contact: self.contact,
      #     department_id: self.department_id,
      #     role: self.role,
      #     created_at: self.created_at
      #   }
      # end
    
  end

  rails_admin do
    label "Doctor"
    label_plural "Doctors"
  
    field :name do
      label "Nama"
    end
    field :email do
      label "Email"
    end
    field :specialization do
      label "Spesialis"
    end
    field :contact do
      label "Kontak"
    end
    field :department_id do
      label "Department"
    end
    field :role do
      label "Role"
    end
    field :created_at do
      label "Dibuat pada"
    end
  end
  
  end
# end
# end
