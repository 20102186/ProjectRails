Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  resources :doctor
  resources :patient
  resources :department
  resources :schedule
  resources :appointment
  resources :medical_history
  post "/login", to: "authentication#authenticate_user"
  resources :posts

end
